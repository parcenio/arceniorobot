package arcenio;

import robocode.HitWallEvent;
import robocode.HitByBulletEvent;
import robocode.BulletHitEvent;
import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;
import static robocode.util.Utils.normalRelativeAngleDegrees;

import java.awt.*;
/**
 * Arcenio - Desenvolvido por Paulo Arcenio com o propósito do desafio da empresa Solutis
 * Github: https://github.com/parcenio
 */
public class Arcenio extends Robot
{

	// Método para encontrar o adversário, sendo no sentido horário ou anti-horário

		public void mira(double Adv) {
			double A=getHeading()+Adv-getGunHeading();
			if (!(A > -180 && A <= 180)) {
				while (A <= -180) {
					A += 360;
				}
				while (A > 180) {
					A -= 360;
				}
			}
			turnGunRight(A);
		}
		
		// Método para identificar se o robô está próximo a parede
		public boolean pertoParede() {
			return (getX() < 50 || getX() > getBattleFieldWidth() - 50 ||
				getY() < 50 || getY() > getBattleFieldHeight() - 50);
		}

		// Método para identificar se o adversário está com baixa energia, consequentemente dar um tiro fatal de maior poder
		public void tiroFatal(double EnergiaIni) {
			double Tiro = (EnergiaIni / 4) + .1;
			fire(Tiro);
		}
		
	

		public void run() {
		
			// Design do robô, suas cores

				setBodyColor(Color.green);
				setGunColor(Color.yellow);
				setRadarColor(Color.gray);
				setBulletColor(Color.green);
				setScanColor(Color.red);

			// Loop infinito onde o robô sempre irá executar	
		 	while(true) {
			 	ahead(150);
				turnGunRight(360);
				back(100);
				turnGunRight(360);
				ahead(100);
				turnRight(100);
				turnGunRight(360);
				
			 }
		 }
		 // Ao escanaer robôs adversários
		 public void onScannedRobot(ScannedRobotEvent e) {
			 
		 		// Vira a mira do canhão para a posição do adversário, checa sua energia para dar um tiro comum ou fatal depois checa novamente o campo de batalha
		 		mira(e.getBearing());
				if(e.getEnergy() < 12) {
					tiroFatal(e.getEnergy());
					
				} else {
					fire(2);
					ahead(100);
					turnGunRight(360);
				}
				scan();
		 }

		 // Ao receber um tiro adversário
		 public void onHitByBullet(HitByBulletEvent e) {
		 	
			// Mira o canhão para a posição do adversário, dar um tiro, anda 100 pixels e escaneia o campo de batalha
			mira(e.getBearing());
			fire(1);
			ahead(100);
			scan();
		 }
		
		// Ao bater com o robô na parede
		 public void onHitWall(HitWallEvent e) {
			// Checa se o robô está próximo a parede e executa movimentos para frente e para trás
			if(pertoParede()) {
				ahead(100);
				back(50);
			} else {
				back(100);
				ahead(50);
			}
		 }

		 // Ao perceber que o robô colidiu com outro
		 public void onHitRobot(HitRobotEvent e) {
		 
			 // Mira o canhão para o adversário, executa um tiro, anda para frente 100 pixels e escaneia o campo de batalha
			mira(e.getBearing());
			fire(3);
			ahead(100);
			scan();
			

		 }
		 

		
	
}
