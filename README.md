# Arcenio Robot

- Robô desenvolvido em java para o desafio da empresa Solutis. 

- Perfil bastante avaliador e agressivo, procura ficar estudando o campo de batalha para depois atirar, mas também tem sua agressividade ao observar o campo de batalhar ou ser colidido por um adversário.

## Cores

- Tem as cores verde,amarelo,cinza e vermelho.
- As cores seguem o predominante verde pois "arsenico" palavra cuja faz menção o robô significa veneno.

## Criador

- Paulo Arcenio
- 25 anos
- Bacharel em Sistemas de Informação
- Amante da tecnologia e do mundo da programação.
- Pronto para abraçar a oportunidade que a Solutis pode me oferecer e crescermos juntos.

[Gitlab](https://gitlab.com/parcenio/arceniorobot)

### Referências

1. https://www.gsigma.ufsc.br/~popov/aulas/robocode/funcoes.html
2. https://robocode.sourceforge.io
3. https://www.ufjf.br/jairo_souza/files/2015/11/RoboCode.pdf